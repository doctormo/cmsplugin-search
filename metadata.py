package_name = 'cmsplugin_search'
name = 'cmsplugin-search'
author = 'Martin Owens via Divio GmbH'
author_email = 'doctormo@gmail.com'
description = "An extension to django CMS to provide multilingual Haystack indexes (fork)"
version = __import__(package_name).__version__
project_url = 'https://gitlab.com/doctormo/cmsplugin-search'
license = 'AGPLv3'
